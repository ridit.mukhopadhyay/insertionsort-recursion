package InsertionSortWithRecursion;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr = {5,3,1,2};
		int length = arr.length;
		displayArray(insertionSort(arr, length, 2));
	}
	
	public static int[] insertionSort(int[] arr,int n,int nOfUnsortedArray) {
		for(int i = nOfUnsortedArray-1;i>0;i--) {
			if(arr[i] < arr[i-1]) {
				int temp = arr[i];
				arr[i] = arr[i-1];
				arr[i-1] = temp;
			}
			else {
				break;
			}
		}
		if(nOfUnsortedArray == n) {
			return arr;
		}
		else {
			return insertionSort(arr, n, nOfUnsortedArray+1);
		}
	}
	public static void displayArray(int[] arr) {
		int length = arr.length;
		for(int i = 0;i<length;i++) {
			if(i == length-1) {
				System.out.print(arr[i]);
			}
			else {
				System.out.print(arr[i] + ",");
			}
		}
	}

}
